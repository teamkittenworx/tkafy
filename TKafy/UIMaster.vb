﻿Public Class UIMaster

    Private mainFrm As Form
    Private dThick As Integer = 2
    Private tSize, lSize, rSize, bSize As Integer

    ''' <summary>
    ''' Gets or sets the thickness of the outside border, default 2
    ''' </summary>
    ''' <value>Thicknes as an integer</value>
    ''' <returns>The thickness as an integer</returns>
    ''' <remarks></remarks>
    Public Property dividerThickness As Integer
        Set(value As Integer)
            dThick = value

        End Set
        Get
            Return dThick

        End Get
    End Property

    ''' <summary>
    ''' Get or set the hight of the top grip (keep in mind that the top grip will have 4 pixels taken from it for the buffer)
    ''' </summary>
    ''' <value>Value as integer</value>
    ''' <returns>Value as integer</returns>
    ''' <remarks></remarks>
    Public Property gripTopHeight() As Integer
        Set(value As Integer)
            tSize = value

        End Set
        Get
            Return tSize
        End Get
    End Property

    ''' <summary>
    ''' Get or set the width of the left grip
    ''' </summary>
    ''' <value>Value as integer</value>
    ''' <returns>Value as integer</returns>
    ''' <remarks></remarks>
    Public Property gripLeftWidth() As Integer
        Set(value As Integer)
            lSize = value

        End Set
        Get
            Return lSize
        End Get
    End Property

    ''' <summary>
    ''' Get or set the width of the right grip
    ''' </summary>
    ''' <value>Value as integer</value>
    ''' <returns>Value as integer</returns>
    ''' <remarks></remarks>
    Public Property gripRightWidth() As Integer
        Set(value As Integer)
            rSize = value

        End Set
        Get
            Return rSize
        End Get
    End Property

    ''' <summary>
    ''' Get or set the hight of the bottom grip
    ''' </summary>
    ''' <value>Value as integer</value>
    ''' <returns>Value as integer</returns>
    ''' <remarks></remarks>
    Public Property gripBottomHeight() As Integer
        Set(value As Integer)
            bSize = value

        End Set
        Get
            Return bSize
        End Get
    End Property

    ''' <summary>
    ''' Sets up the default properties of the form... Requires a form as input
    ''' </summary>
    ''' <param name="frm">The form that you wish to TKafy</param>
    ''' <param name="goWithDefaults">If set to true it will then create the form with the default values</param>
    ''' <remarks></remarks>
    Public Sub New(frm As Form, Optional goWithDefaults As Boolean = False)
        ' set the global
        mainFrm = frm

        ' set the form's border style
        frm.FormBorderStyle = FormBorderStyle.None

        If goWithDefaults Then
            tSize = 28
            bSize = 64
            lSize = 4
            rSize = 4

            createUI()

        End If
    End Sub

    ''' <summary>
    ''' Creates the new UI
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub createUI()
        ' Main loop
        pushControls()
        createBorders()

        Dim nUIStuff As New UIStuff(mainFrm)
    End Sub

    Private Sub createBorders()
        ' ============================================================== Border
        ' make the panels for the borders
        Dim pnlBTop, pnlBLeft, pnlBRight, pnlBBottom As New Panel

        ' create an array and add all the panels
        Dim borderPnls() As Panel = {pnlBTop, pnlBLeft, pnlBRight, pnlBBottom}

        ' set each panel's individual properties
        For i = 0 To borderPnls.GetUpperBound(0)
            Dim pnlDock As DockStyle

            Select Case i
                Case 0
                    pnlDock = DockStyle.Top

                Case 1
                    pnlDock = DockStyle.Left

                Case 2
                    pnlDock = DockStyle.Right

                Case 3
                    pnlDock = DockStyle.Bottom

            End Select

            borderPnls(i).Dock = pnlDock

        Next

        ' for each panel, set the properties
        For Each pnl As Panel In borderPnls
            With pnl
                .Width = dThick
                .Height = dThick
                .BackColor = Color.Black

            End With

            ' and add them to the form
            mainFrm.Controls.Add(pnl)
        Next

        ' ============================================================== Main Border
        ' create the main panel
        Dim pnlMain As New Panel

        ' set it's properties
        With pnlMain
            .Dock = DockStyle.Fill

        End With

        ' add it to the form
        mainFrm.Controls.Add(pnlMain)

        ' ============================================================== Grips
        ' make a bunch of panels
        Dim pnlTopBuffer, pnlTop, pnlLeft, pnlRight, pnlBottom As New Panel

        ' create a panel array and add each panel to array
        Dim pnls() As Panel = {pnlTopBuffer, pnlTop, pnlLeft, pnlRight, pnlBottom}

        ' set individual properties (for this set of panels, doing it this way is better)
        With pnlTopBuffer
            .Height = 4
            .Dock = DockStyle.Top

        End With

        With pnlTop
            .Height = tSize - 4
            .Dock = DockStyle.Top

        End With

        With pnlLeft
            .Width = lSize
            .Dock = DockStyle.Left

        End With

        With pnlRight
            .Width = rSize
            .Dock = DockStyle.Right

        End With

        With pnlBottom
            .Height = bSize
            .Dock = DockStyle.Bottom

        End With

        ' for each panel in array
        For Each pnl As Panel In pnls
            With pnl
                ' set it's global properties
                .AccessibleRole = AccessibleRole.Grip
                .BackColor = Color.BlueViolet

            End With

            ' add the current panel to the form
            pnlMain.Controls.Add(pnl)
        Next

        ' ...Profit?

        ' ==============================================================
    End Sub

    Private Sub pushControls()

        Dim mainFrmControls As New List(Of Control)

        mainFrm.Width += (lSize + rSize)
        mainFrm.Height += (tSize + bSize)

        For Each c As Control In mainFrm.Controls
            c.Left += lSize + 2
            c.Top += tSize + 2

        Next


    End Sub

End Class
