﻿''' <summary>
''' Read the comments in the UIStuff class for details of what you can do! Trust me on that!
''' </summary>
''' <remarks></remarks>
Public Class UIStuff

    Private frm As Form
    Private lBorderContList As New List(Of Control)
    Private lCloseContList As New List(Of Control)
    Private lMinContList As New List(Of Control)
    Private pLoc As Point

    '=====================================================================================================================
    '
    ' Any control you give the "Accessible Role" of -
    '                                                   "Grip" will allow the user to move the window around.
    '                                                   "PushButton" and a tag of -
    '                                                                             "close" will close the window
    '                                                                             "min" will minimise the window
    '
    ' Don't matter what type of control it is, just as long as it can have it's accessible role set...
    '
    '=====================================================================================================================

    Sub New(ByVal sender As Form)
        frm = sender
        UILoad()

    End Sub

    ''' <summary>
    ''' The Load command. Please see the UIStuff class for details...
    ''' </summary>
    ''' <remarks>Try to run something that will auto do the forms it self? Maybe something on the "New" Event</remarks>
    ''' 
    Private Sub UILoad()

        ' Put in the "OnLoad" event of the form... But first you will need to dim a new "UIStuff"
        ' I.e.
        ' 
        '  - dim UIStuff AS new UIStuff
        ' 
        ' as a global and then in your onload event of the form put
        ' 
        '  - UIStuff.UILoad(me)
        '
        '  --- Kitten


        For Each control In frm.Controls
            Try
                If control.AccessibleRole = Windows.Forms.AccessibleRole.Grip Then

                    If Not lBorderContList.Contains(control) Then
                        lBorderContList.Add(control)

                    End If
                End If

                If control.AccessibleRole = Windows.Forms.AccessibleRole.PushButton Then

                    If Not lBorderContList.Contains(control) Then
                        If control.tag = "close" Then
                            lCloseContList.Add(control)

                        End If

                        If control.tag = "min" Then
                            lMinContList.Add(control)

                        End If
                    End If
                End If


                If control.controls.count > 0 Then
                    getControlChildren(control)

                End If

            Catch ex As Exception
                MessageBox.Show("ERROR" & vbNewLine & ex.Message)
            End Try
        Next

        Try
            For Each control In lBorderContList
                AddHandler control.MouseDown, AddressOf Me.moverDown
                AddHandler control.MouseMove, AddressOf Me.moverMove

            Next

            For Each control In lCloseContList
                AddHandler control.Click, AddressOf Me.closerClick

            Next

            For Each control In lMinContList
                AddHandler control.Click, AddressOf Me.MinimClick

            Next

        Catch ex As Exception
            MessageBox.Show("ERROR" & vbNewLine & ex.Message)

        End Try

    End Sub

    ' ===== PRIVATES
    'A recursive sub to get any children of children!
    Private Sub getControlChildren(ByVal control As Control)
        For Each controlChild As Control In control.Controls
            If controlChild.AccessibleRole = Windows.Forms.AccessibleRole.Grip Then
                If Not lBorderContList.Contains(controlChild) Then
                    lBorderContList.Add(controlChild)

                End If
            End If

            If controlChild.AccessibleRole = Windows.Forms.AccessibleRole.PushButton Then
                If Not lBorderContList.Contains(controlChild) Then
                    If controlChild.Tag = "close" Then
                        lCloseContList.Add(controlChild)

                    End If

                    If controlChild.Tag = "min" Then
                        lMinContList.Add(controlChild)

                    End If

                End If
            End If

            If controlChild.Controls.Count > 0 Then
                getControlChildren(controlChild)

            End If
        Next
    End Sub

    'Registers that the left mouse button is down.
    Private Sub moverDown(ByVal sender As Object, ByVal e As MouseEventArgs)
        If e.Button = Windows.Forms.MouseButtons.Left Then
            pLoc = e.Location

        End If
    End Sub

    'Moves the window when the mouse is dragged with the left mouse button down.
    Private Sub moverMove(ByVal sender As Object, ByVal e As MouseEventArgs)
        If e.Button = Windows.Forms.MouseButtons.Left Then
            frm.Location += e.Location - pLoc

        End If
    End Sub

    'Closes a window. Can be edited to give more functionality to the closing of a window!
    Private Sub closerClick(ByVal sender As Object, ByVal e As System.EventArgs)
        frm.Close()

    End Sub

    'Checks to see if the window is normal or not and acts accordingly.
    Private Sub MinimClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If frm.WindowState = FormWindowState.Normal Then
            frm.WindowState = FormWindowState.Minimized

        Else
            frm.WindowState = FormWindowState.Normal

        End If


    End Sub

End Class
